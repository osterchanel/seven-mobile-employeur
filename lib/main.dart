import 'package:flutter/material.dart';
import 'package:seven_sales/Screens/Employeur/connexion.dart';
import 'package:seven_sales/Screens/Employeur/coordonnees.dart';
import 'package:seven_sales/Screens/Employeur/password.dart';



void main() => runApp(const MyApp());


class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      home: Connect(),
    );
  }
}


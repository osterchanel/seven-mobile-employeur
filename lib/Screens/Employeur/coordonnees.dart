import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:seven_sales/Screens/Employeur/password.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:flutter/services.dart';


class Coordonnees extends StatefulWidget {
  const Coordonnees({super.key});

  @override
  State<Coordonnees> createState() => _CoordonneesState();
}

class _CoordonneesState extends State<Coordonnees> {
  bool isChecked = false;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final TextEditingController controller = TextEditingController();
  String initialCountry = 'FR';
  PhoneNumber number = PhoneNumber(isoCode: 'FR');
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child:Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.red,

          leading:IconButton(
                    icon: Icon(Icons.arrow_back_ios_new, size: 20.0,),
                    onPressed: () {
                       Navigator.of(context).pop();
                    },
                    color: Colors.white,
                  ) ,
                  
        ),
        body:Padding (padding: EdgeInsets.all(30),

        child :Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
           
             Center(
              
              child:Row(
                mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 3,
                  width: 70,
                  color: Colors.red,
                ),
                SizedBox(width: 3,),
                Container(
                  height: 3,
                  width: 70,
                  color: Colors.grey,
                ),
                SizedBox(width: 3,),
                Container(
                  height: 3,
                  width: 70,
                  color: Colors.grey,
                ),
                SizedBox(width: 3,),
                Container(
                  height: 3,
                  width: 70,
                  color: Colors.grey,
                )
              ],
             ) ,
             ),

             


             SizedBox(height: 20,),

             Text('Vos coordonnées',
             style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
             ),),

             SizedBox(height: 20,),
             Expanded(child: Center(child: Column(
              children: [
                TextFormField(
                  cursorColor: Colors.black,
                  validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Ce champ est obligatoire';
                  }
                  return null;
                },
              style: TextStyle( color: Colors.black),
              decoration: InputDecoration(
                  hintText: ' Email',
                  prefixIcon: Icon(Icons.email, color: Colors.grey,),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide  (color: Colors.black,),
                    
                    ),
                    focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide  (color: Colors.black,),
                    
                    ),

                    
                ),
                ),
                SizedBox(height: 10,),

                InternationalPhoneNumberInput(
                  
                  cursorColor: Colors.black,
                    onInputChanged: (PhoneNumber number) {
                      
                      print(number.phoneNumber);
                    },
                    onInputValidated: (bool value) {
                      print(value);
                    },
                    selectorConfig: SelectorConfig(
                      selectorType: PhoneInputSelectorType.BOTTOM_SHEET,
                    ),
                    
                    ignoreBlank: false,
                    autoValidateMode: AutovalidateMode.disabled,
                    selectorTextStyle: TextStyle(color: Colors.black),
                    initialValue: number,
                    textFieldController: controller,
                    formatInput: true,
                    keyboardType:TextInputType.numberWithOptions(signed: true, decimal: true),
                    
                    inputBorder: OutlineInputBorder(),
                    onSaved: (PhoneNumber number) {
                      print('On Saved: $number');
                    },
                    inputDecoration: InputDecoration(
                    border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide  (color: Colors.black,),
                          
                          ),
                          focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide  (color: Colors.black,),
                          
                          ),

                    ),
                    
                  ),
            


                 Row(
                  children: [
                 Checkbox(
                    value: isChecked,
                    onChanged: (value) {
                      setState(() {
                        isChecked = value!;
                      });
                    },
                    activeColor: Colors.red,
                   
                  ),
                  SizedBox(
                    width: 2,
                  ),
                  
                  Padding(padding: EdgeInsets.only(
                  top: 55,
                 ),
                 child: Text('Je souhaite recevoir des actualités du\nsecteur et les conseils de Seven JOB par\nemail.Je peux me désabonner à\ntout moment.',
                 style: TextStyle(fontSize: 15),),
                 ),
                  
                  ],
                 ),

              ],
             ),)),

             
                

                
                
                 Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      ElevatedButton(onPressed:(){
                        Navigator.push(context, 
                        MaterialPageRoute(builder:(context)=>Password()));
                      }, 
                        style: ElevatedButton.styleFrom(
                        elevation: 0,
                       primary: Colors.red,
                       minimumSize: Size(200, 50)
                      
                      ),
                      
                child: Text('Suivant',
                       style: TextStyle(
                        fontSize: 15
                       ),
                )),
                    ],
                  ),
                 )


                
        
                
                
                   

               
                  

                  
                 
                   

          ],
        ) )
        ) );
  }

}

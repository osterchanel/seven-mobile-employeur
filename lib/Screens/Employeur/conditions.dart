import 'package:flutter/material.dart';
import 'package:seven_sales/Screens/Employeur/Site1.dart';

class Condition extends StatefulWidget {
  const Condition({super.key});

  @override
  State<Condition> createState() => _ConditionState();
}

class _ConditionState extends State<Condition> {
  @override
  List <bool> isChecked = [false,false];
  bool isAnyChecked() {
      for (var i = 0; i < isChecked.length; i++) {
        if (isChecked[i]) {
          return true;
        }
      }
      return false;
    }
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.red,

          leading:IconButton(
                    icon: Icon(Icons.arrow_back_ios_new, size: 20.0,),
                    onPressed: () {
                       Navigator.of(context).pop();
                    },
                    color: Colors.white,
                  ) ,
                  
        ),
        body:Padding (padding: EdgeInsets.all(30),

        child :Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
           
             SizedBox(height: 10,),

             Center(
              
              child:Row(
                mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 3,
                  width: 70,
                  color: Colors.red,
                ),
                SizedBox(width: 3,),
                Container(
                  height: 3,
                  width: 70,
                  color: Colors.red ,
                ),
                SizedBox(width: 3,),
                Container(
                  height: 3,
                  width: 70,
                  color: Colors.red ,
                ),
                SizedBox(width: 3,),
                Container(
                  height: 3,
                  width: 70,
                  color: Colors.grey,
                )
              ],
             ) ,
             ),

             


             SizedBox(height: 20,),

             Text("Nos conditions d'utilisation",
             style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
             ),),

           
             Expanded(
              child: Center(
                child: Column(
              children: [

                 Row(
                  children: [
                    Checkbox(value: isChecked[0],
                          onChanged: (bool? value){
                  setState(() {
                    isChecked[0] = value!;
                  });
                },
                shape: CircleBorder(),
                activeColor:Colors.red,
                    ),
                    Padding(padding: EdgeInsets.only(top: 54),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text("J'accepte les ",
                 style: TextStyle(fontSize: 17),),
                 InkWell(
                      onTap: () {
                      },
                      child: Text("Conditions Générales",
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.bold
                      ),),
                    ),
                          ],
                        ),
                        Row(
                  children: [
                 InkWell(
                      onTap: () {
                      },
                      child: Text("d'Utilisation de Seven JOB",
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.bold
                      ),),
                    ),
                    Text(", les ",
                 style: TextStyle(fontSize: 17),),

                 InkWell(
                      onTap: () {
                      },
                      child: Text("Conditions",
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.bold
                      ),),
                    ),
                  
                  ],
                 ),
                 Row(
                  children: [
                 InkWell(
                      onTap: () {
                      },
                      child: Text("Générales d'Utilisation de Stripe",
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.bold
                      ),),
                    ),
                    Text(", et les ",
                 style: TextStyle(fontSize: 17),),
                  ],
                 ),
                  Row(
                  children: [
                 InkWell(
                      onTap: () {
                      },
                      child: Text("Générales d'Utilisation de Stripe Connect",
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.bold
                      ),),
                    ),
                    
                  ],
                 ),
                      ],
                    ),)
                   
                  
                  ],
                 ),
                 SizedBox(
                  height: 10,
                 ),

                 Row(
                  children: [
                     Checkbox(value: isChecked[1], 
                     onChanged:(bool? value){
                      setState(() {
                       isChecked[1] = value!;
                      });
                    },
                     shape: CircleBorder(),
                     activeColor: Colors.red,
                    ),
               
                    
                    Padding(padding: EdgeInsets.only(top: 15),
                    child :Column(
                      
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text("J'accepte la ",
                 style: TextStyle(fontSize: 17),),
                 InkWell(
                      onTap: () {
                      },
                      child: Text("Politique de confidentialité",
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.bold
                      ),),
                    ),
                          ],
                        ),
                        Row(
                  children: [
                 InkWell(
                      onTap: () {
                      },
                      child: Text("de Seven JOB",
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.bold
                      ),),
                    ),
                  
                  ],
                 ),
                 
      
                      ],
                    ), ),
                    
                   
                  
                  ],
                 ),
                  

              ],
             ),)),

             
                

                
                
                 Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      ElevatedButton(onPressed:(){
                        Navigator.push(context, 
                        MaterialPageRoute(builder: (context)=> Site1()));
                        
                      }, 
                        style: ElevatedButton.styleFrom(
                        elevation: 0,
                       primary: Colors.red,
                       minimumSize: Size(200, 50)
                     
                      ),
                      
                child: Text('Suivant',
                       style: TextStyle(
                        fontSize: 15
                       ),
                )),
                    ],
                  ),
                 )


                
        
                
                
                   

               
                  

                  
                 
                   

          ],
        ) ),));
  }
}
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:seven_sales/Screens/Employeur/conditions.dart';
import 'package:seven_sales/Screens/Employeur/mdpoublie.dart';



class Password extends StatefulWidget {
  const Password({super.key});

  @override
  State<Password> createState() => _PasswordState();
}

class _PasswordState extends State<Password> {
  @override
  bool _isSecret = true;

  Widget build(BuildContext context) {
   

    return  SafeArea(
      child:Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.red,

          leading:IconButton(
                    icon: Icon(Icons.arrow_back_ios_new, size: 20.0,),
                    onPressed: () {
                       Navigator.of(context).pop();
                    },
                    color: Colors.white,
                  ) ,
                  
        ),
        body: Padding(padding: EdgeInsets.all(30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

    

             Center(
              
              child:Row(
                mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 3,
                  width: 70,
                  color: Colors.red,
                ),
                SizedBox(width: 3,),
                Container(
                  height: 3,
                  width: 70,
                  color: Colors.red,
                ),
                SizedBox(width: 3,),
                Container(
                  height: 3,
                  width: 70,
                  color: Colors.grey,
                ),
                SizedBox(width: 3,),
                Container(
                  height: 3,
                  width: 70,
                  color: Colors.grey,
                )
              ],
             ) ,
             ),

             


             SizedBox(height: 20,),
             Expanded(child: Center( child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Votre mot de passe',
             style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
             ),),

             SizedBox(height: 20,),

             TextFormField(
              obscureText: _isSecret,
              cursorColor: Colors.black,
              style: TextStyle( color: Colors.black),
              decoration: InputDecoration(
                  
                  hintText: 'Password',

                  suffixIcon: InkWell(
                     
                      onTap: () =>
                       setState(() => 
                        _isSecret = !_isSecret,
                      ),
                      child: Icon(!_isSecret
                      ?Icons.visibility_off
                      : Icons.visibility,
                      color: Colors.grey,),
                    ),
                    

                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide  (color: Colors.black,),
                    
                    
                    ),
                    focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide  (color: Colors.black,),
                    
                    ),

                    
                ),
                ),


              ],
             ))),
                 Center(
                  
                  child:Column(
                    
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                    
                      ElevatedButton(onPressed:(){
                        Navigator.push(context, 
                        MaterialPageRoute(builder: (context)=>Condition()));
                      }, 
                      style: ElevatedButton.styleFrom(
                       elevation: 0,
                       primary: Colors.red,
                       minimumSize: Size(200, 50)
                      ),
                      
                child: Text('Suivant',
                       style: TextStyle(
                        fontSize: 15
                       ),
                ))
                    ],
                  ) ,
                 ),

                  
                 
                   

          ],
        ),),
        
        
      ) );
  }
}
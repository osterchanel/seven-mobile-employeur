import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:seven_sales/Screens/Employeur/Mission.dart';
import 'package:seven_sales/Screens/Employeur/conditions.dart';
import 'package:seven_sales/Screens/Employeur/mdpoublie.dart';
import 'package:flutter/services.dart';



class mdp_connexion extends StatefulWidget {
  const mdp_connexion({super.key});

  @override
  State<mdp_connexion> createState() => _mdp_connexionState();
}

class _mdp_connexionState extends State<mdp_connexion> {
  @override
  bool _isSecret = true;
  String _password ="";
  
  Widget build(BuildContext context) {
   

    return  SafeArea(
      child:Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.red,

          leading:IconButton(
                    icon: Icon(Icons.arrow_back_ios_new, size: 20.0,),
                    onPressed: () {
                       Navigator.of(context).pop();
                    },
                    color: Colors.white,
                  ) ,
                  
        ),
        body: Padding(padding: EdgeInsets.all(30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

           
            
             SizedBox(height: 20,),

             Expanded(child: Center( child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text('Votre mot de passe',
             style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
             ),),

             SizedBox(height: 20,),

             TextFormField(
              obscureText: _isSecret,
              cursorColor: Colors.black,
              style: TextStyle( color: Colors.black),
              validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Ce champ est obligatoire';
                  }
                  return null;
                },
              decoration: InputDecoration(
                  
                  hintText: 'Password',
                  prefixIcon: Icon(Icons.lock, color: Colors.grey,),

                  suffixIcon: InkWell(
                     
                      onTap: () =>
                       setState(() => 
                        _isSecret = !_isSecret,
                      ),
                      child: Icon(!_isSecret
                      ?Icons.visibility_off
                      : Icons.visibility,
                      color: Colors.grey,),
                    ),
                    

                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide  (color: Colors.black,),
                    
                    
                    ),
                    focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide  (color: Colors.black,),
                    
                    ),

                    
                ),
                ),


              ],
             ))),
                 Center(
                  
                  child:Column(
                    
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      InkWell(
                      onTap: () {
                       Navigator.push(context, 
                       MaterialPageRoute(builder: (context)=>Forget()));
                       
                      },
                      child: Text("Vous aviez oublié votre mot de passe",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        color : Colors.black,
                        fontSize:   15
                      ),),
                    ),
                    SizedBox(height: 10,),
                    
                      ElevatedButton(onPressed:(){
                        Navigator.push(context, 
                        MaterialPageRoute(builder: (context)=>Mission()));
                      }, 
                      style: ElevatedButton.styleFrom(
                       elevation: 0,
                       primary: Colors.red,
                       minimumSize: Size(200,50),
                       
                      ),
                      
                child: Text('Suivant',
                       style: TextStyle(
                        fontSize: 15
                       ),
                ))
                    ],
                  ) ,
                 ),

                  
                 
                   

          ],
        ),),
        
        
      ) );
  }
}
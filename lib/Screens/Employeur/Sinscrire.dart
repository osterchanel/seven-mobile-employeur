import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:seven_sales/Screens/Employeur/connexion.dart';
import 'package:seven_sales/Screens/Employeur/coordonnees.dart';
import 'package:flutter/services.dart';




class Sinscrire extends StatefulWidget {
  const Sinscrire({super.key});

  @override
  State<Sinscrire> createState() => _SinscrireState();
}

class _SinscrireState extends State<Sinscrire> {
  
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child:Scaffold( 
        appBar: AppBar(
          backgroundColor: Colors.red,

          leading:IconButton(
                    icon: Icon(Icons.arrow_back_ios_new, size: 20.0,),
                    onPressed: () {
                       Navigator.of(context).pop();
                    },
                    color: Colors.white,
                  ) ,
                  
        ),
        body: Padding(
            padding: EdgeInsets.all(20),
            child:Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
            
                 Text("S'inscrire pour trouver du renfort",
                 style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                 ),),
                 
               
                Expanded(
                  child:Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    SizedBox(
                  height: 40,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context, 
                        MaterialPageRoute(builder: (context) => Coordonnees()),
                      ); 
                    },
                    child: 
                        Text("M'inscrire avec mon adresse email",
                        textAlign : TextAlign.center,
                    ),
                    
                    style: ElevatedButton.styleFrom(
                      primary: Colors.red,
                     onPrimary: Colors.white,
                   
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                        
                      ), 
                    ),
                  ),
                ),
                 
                   SizedBox(
                   height: 30,
                ),
                Center(
                  child:Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: 120,
                            height: 1,
                            color: Colors.grey,
                          ),
                          SizedBox(width: 10),
                          Text(
                            "ou",
                            style: TextStyle(fontSize: 15),
                          ),
                          SizedBox(width: 10),
                          Container(
                            width: 120,
                            height: 1,
                            color: Colors.grey,
                          ),
                        ],
                      ), 
                ),
                  


                 SizedBox(
                   height: 20,
                ),
                  
                
                 SizedBox(
                  height: 40,
                  child: ElevatedButton(
                    onPressed: () {},
                    style: ElevatedButton.styleFrom(
                      primary: Theme.of(context).dialogBackgroundColor,
                    ),
                    
                    child: Center(
                      child: Row( 
                        mainAxisAlignment: MainAxisAlignment.center,
                        children : [
                        Image.network('https://cdn.dribbble.com/users/2522374/screenshots/7911727/google-logo.png',
                        height: 40,),
                       
                        Text("M'inscrire avec Google",
                        style: TextStyle(
                          color: Colors.black,
                        ),)
                      ],),),
                  ),
                ),
                 
                   SizedBox(
                   height: 20,
                ),
                
                                   SizedBox(
                  height: 40,
                  child: ElevatedButton(
                    onPressed: () {},
                    style: ElevatedButton.styleFrom(
                      primary: Theme.of(context).dialogBackgroundColor,
                    ),
                 
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.network('https://static.vecteezy.com/system/resources/previews/017/221/833/non_2x/apple-logo-free-png.png',
                        height: 50,),
                       
                        Text("M'inscrire avec Apple",
                        style: TextStyle(
                          color: Colors.black,
                        ),)
                      ],),
                  ),
                ),
                  ],

                ),
                    ],
                  ),
                )),
                
                
                
                   
                 Align(
                      alignment: Alignment.bottomCenter,
                      child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                  
                  children: [
                    Text("Vous avez déjà un compte ?"), SizedBox(width: 1,),

                    InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Connect()),
                        );
                      },
                      child: Text("Connectez-vous ici",
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                      ),),
                    )

                  ],
                )
                      // vos widgets ici
                    ],
                  ),
                ),
                    ),

                 
                

                
                 
                
                
              ],
            ) ,),
       
   
    )) ;
  }
}
import 'package:flutter/material.dart';
import 'package:seven_sales/Screens/Employeur/Mailenvoye.dart';

class Forget extends StatefulWidget {
  const Forget({super.key});

  @override
  State<Forget> createState() => _ForgetState();
}

class _ForgetState extends State<Forget> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.red,

          leading:IconButton(
                    icon: Icon(Icons.arrow_back_ios_new, size: 20.0,),
                    onPressed: () {
                       Navigator.of(context).pop();
                    },
                    color: Colors.white,
                  ) ,
                  
        ),
        body: Padding(padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            
             Expanded(child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text('Vous aviez oublié votre mot de passe',
             style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
             ),),

             SizedBox(height: 10,),
             Text("Renseignez l'adresse email associée à votre\ncompte et nous vous enverrons un lien pour\nréinitialiser votre mot de passe.",
             style: TextStyle(color: Colors.grey, fontSize: 15),),

             SizedBox(height: 10,),

             TextFormField(
              style: TextStyle( color: Colors.black),
              decoration: InputDecoration(
                  hintText: 'Email',
                  prefixIcon: Icon(Icons.mail, color: Colors.grey,),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide  (color: Colors.black,),
                    
                    
                    ),
                    focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide  (color: Colors.black,),
                    
                    ),

                    
                ),
                ),

              ],
             )),

             

               
               


        
                Column(
                    
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    
                    children: [
                      
                   
                    
                      ElevatedButton(onPressed:(){
                        Navigator.push(context, 
                        MaterialPageRoute(builder: (context) => Mail_send()));
                      }, 
                      style: ElevatedButton.styleFrom(
                       elevation: 0,
                       primary: Colors.red,
                       minimumSize: Size(200, 50)
                      ),
                      
                child: Text('Suivant',
                       style: TextStyle(
                        fontSize: 15
                       ),
                ))
                    ],
                  ) ,
              

                  
                 
          ],
        ),),
      ));
  }
}
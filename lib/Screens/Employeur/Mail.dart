import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:seven_sales/Screens/Employeur/mdp_connexion.dart';
import 'package:seven_sales/Screens/Employeur/password.dart';


class Mail extends StatefulWidget {
  const Mail({super.key});

  @override
  State<Mail> createState() => _MailState();
}

class _MailState extends State<Mail> {
  bool isChecked = false;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child:Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.red,

          leading:IconButton(
                    icon: Icon(Icons.arrow_back_ios_new, size: 20.0,),
                    onPressed: () {
                       Navigator.of(context).pop();
                    },
                    color: Colors.white,
                  ) ,
                  
        ),
        body:Padding (padding: EdgeInsets.all(30),

        child :Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            

             


             SizedBox(height: 20,),

             Text('Votre adresse email',
             style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
             ),),

             SizedBox(height: 20,),
             Expanded(child: Center(child: Column(
              children: [
                TextFormField(
                  validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Ce champ est obligatoire';
                  }
                  return null;
                },
              style: TextStyle( color: Colors.black),
              decoration: InputDecoration(
                  hintText: ' Email',
                  prefixIcon: Icon(Icons.email, color: Colors.grey,),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide  (color: Colors.black,),
                    
                    ),
                    focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide  (color: Colors.black,),
                    
                    ),

                    
                ),
                ),
              ],
             ),)),

             
                

                
                
                 Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      ElevatedButton(onPressed:(){
                        Navigator.push(context, 
                        MaterialPageRoute(builder:(context)=>mdp_connexion()));
                      }, 
                        style: ElevatedButton.styleFrom(
                        elevation: 0,
                       primary: Colors.red,
                       minimumSize: Size(200, 50)
                      
                      ),
                      
                child: Text('Suivant',
                       style: TextStyle(
                        fontSize: 15
                       ),
                )),
                    ],
                  ),
                 )


                
        
                
                
                   

               
                  

                  
                 
                   

          ],
        ) )
        ) );
  }
}